import { Row, Col } from 'react-bootstrap';
import { motion, useAnimation } from 'framer-motion';
import { useCallback, useEffect } from 'react';

import { ReactComponent as Welcome } from '../../assets/svg/welcome.svg';
import { ReactComponent as Logo } from '../../assets/svg/logo.svg';
import "../../assets/styles/login.scss"
import LoginForm from './LoginForm';


const Login = () => {
    // Import du hook useAnimation
    const controls = useAnimation();

    //Fonction pour animer un composant en utilisant Framer Motion.
    const animate = useCallback(async () => {

        await controls.start({ y: 0, opacity: 1 });
    }, [controls]);


    // Utilisation de "useEffect" pour déclencher l'animation une fois que le composant est monté
    useEffect(() => {
        animate();
    }, [animate]);

    return (
        <div className='login-container mx-0'>
            <Row>
                <Col md={12} className="text-center">
                    <motion.h1
                        initial={{ y: -20, opacity: 0 }}
                        animate={controls}
                        transition={{ duration: 1, delay: 0.5 }}
                    >
                        <Logo />
                    </motion.h1>
                </Col>
            </Row>
            <Row className='mt-5 mb-5'>
                <Col md={6} className='mt-5'>
                    <Row className='justify-content-center text-center'>
                        <motion.div
                            initial="initial"
                            animate="animate"
                            variants={{
                                initial: { x: -50, opacity: 0 },
                                animate: { x: 0, opacity: 1 },
                            }}
                            transition={{ duration: 2, delay: 0.5 }}
                        >
                            <Welcome />
                        </motion.div>
                        <Col md={4} className='mt-5 ms-5'>
                            <motion.div
                                initial="initial"
                                animate="animate"
                                variants={{
                                    initial: { y: 50, opacity: 0 },
                                    animate: { y: 0, opacity: 1 },
                                }}
                                transition={{ duration: 2, delay: 0.5 }}
                            >
                                <LoginForm />
                            </motion.div>
                        </Col>
                    </Row>

                </Col>
                <Col md={2}></Col>
                <Col md={4} className="image-container d-flex justify-content-end">
                    <motion.img
                        src={require('../../assets/images/eiffel.png')}
                        alt="eiffel"
                        className='image float-right'
                        initial={{ x: '100%', opacity: 0 }}
                        animate={{ x: 0, opacity: 1 }}
                        transition={{ duration: 2, delay: 0.5 }}
                    />
                </Col>
            </Row>
        </div>
    )

}
export default Login