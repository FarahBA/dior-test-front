import { useState } from 'react';
import { Button } from 'react-bootstrap';
import { useNavigate } from 'react-router-dom';
import { useMutation } from '@apollo/client';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import * as Yup from 'yup';

import { API_URL } from '../../graphql/config';
import { LOGIN_USER } from '../../graphql/mutations';

const LoginForm = () => {
    // Déclaration des variables d'état
    const [errorMessage, setError] = useState();

    // Hook pour la navigation entre les pages
    const navigate = useNavigate();

    // Utilisation du hook useMutation pour appeler la mutation "loginUser" (GraphQL)
    const [loginUser, { error }] = useMutation(LOGIN_USER);

    // Définition du schéma de validation pour les champs de formulaire
    const validationSchema = Yup.object().shape({
        username: Yup.string().required('Champ obligatoire'),
        password: Yup.string().required('Champ obligatoire'),
    });

    // Initialisation des valeurs par défaut pour les champs de formulaire
    const initialValues = {
        username: '',
        password: '',
    };


    /**
     * Définition de la fonction de soumission du formulaire 
     * @param {Object} values
     * @param {Function} setSubmitting - Une fonction pour gérer le statut de soumission du formulaire.
    */
    const handleSubmit = async (values, { setSubmitting }) => {
        // Réinitialisation de toute erreur précédente
        setError();

        // Appel de la mutation GraphQL "loginUser" pour l'authentification
        await loginUser({
            variables: values,
            context: {
                uri: `${API_URL}/login`,
                method: 'POST',
            },
        })
            .then(() => {
                navigate('/Home'); // Redirection vers la page '/Home' en cas de succès de l'authentification
            })
            .catch(() => {
                setError('Your login or password are incorrect'); // Gestion des erreurs d'authentification
            })
            .finally(() => {
                setSubmitting(false); // Réinitialisation du statut de soumission du formulaire
            });
    };
    
    return (
        <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={handleSubmit}

        >
            {({ isSubmitting, isValid }) => (
                <Form className="mt-5 ms-5">
                    <div className="form-group mt-5">
                        {error && <p className="danger" >{errorMessage}</p>}

                        <Field
                            type="login"
                            name="username"
                            id="username"
                            placeholder="LOGIN"
                            className={`control ${(!isValid || error) ? 'border-red' : ''}`}
                        />
                        <ErrorMessage name="username" component="div" className="danger" />
                    </div>

                    <div className="form-group">
                        <Field
                            type="password"
                            name="password"
                            id="password"
                            placeholder="PASSWORD"
                            className={`control ${(!isValid || error) ? 'border-red' : ''}`}
                        />
                        <ErrorMessage name="password" component="div" className="danger" />
                    </div>

                    <Button type="submit" className='btn-login' disabled={isSubmitting}>LOGIN</Button>

                </Form>
            )}
        </Formik>)
}

export default LoginForm