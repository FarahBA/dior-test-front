import { Row, Col, Button, Dropdown } from 'react-bootstrap';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { useCallback, useEffect, useState } from 'react';
import { RiWomenLine } from 'react-icons/ri';
import { AiOutlineWoman } from 'react-icons/ai';
import { LiaTshirtSolid } from 'react-icons/lia';
import { HiOutlineShoppingBag } from 'react-icons/hi2';
import { GiBigDiamondRing } from 'react-icons/gi';
import { motion, useAnimation } from 'framer-motion';

import { ReactComponent as Logo } from '../../assets/svg/logo.svg';
import { ReactComponent as CartLogo } from '../../assets/svg/cart.svg';
import { API_URL } from '../../graphql/config';
import "../../assets/styles/home.scss"
import ProductSlider from './ProductSlider';
import Cart from './Cart';

const Home = () => {
    // État initial des composants
    const [show, setShow] = useState(false); // État pour afficher/masquer un composant
    const [products, setProducts] = useState([]); // État pour stocker la liste des produits
    const [totalProduct, setTotalProduct] = useState(0); // État pour suivre le total des produits dans le panier

    // Chargement la liste des produits depuis l'API.     
    const loadProducts = async () => {
        try {
            const response = await fetch(`${API_URL}/products`, {
                method: 'GET',
            });
            const data = await response.json();
            setProducts(data);
        } catch (error) {
            console.error('Erreur lors du chargement des produits:', error);
        }
    };

    // Animation pour afficher un composant avec Framer Motion
    const controls = useAnimation();

    //Fonction pour animer un composant en utilisant Framer Motion.
    const animate = useCallback(async () => {

        await controls.start({ y: 0, opacity: 1 });
    }, [controls]);


    // Déclencher l'animation lorsque le composant est monté
    useEffect(() => {
        animate();
    }, [animate]);

    // Charger la liste des produits lorsque le composant est monté
    useEffect(() => {
        loadProducts();
    }, []);

    //Ajout d'un produit au panier.
    const addToCart = () => {
        handleShow();
        setTotalProduct((prev) => prev + 1);
    };

    //Afficher le cart en utilisant setShow.
    const handleShow = () => setShow(true);

    return (
        <div className='home-container'>
            <Row>
                <Col md={12} >
                    <motion.h1
                        initial={{ y: -20, opacity: 0 }}
                        animate={controls}
                        transition={{ duration: 2, delay: 0.5 }}
                    >
                        <div className="d-flex justify-content-between">
                            <div >

                                <Logo className="logo" />

                            </div>
                            <div className='d-flex flex-row cart'>
                                <p className='mt-4 total'>{totalProduct}</p> <CartLogo />
                            </div>
                        </div>
                    </motion.h1>

                </Col>
            </Row>
            <Row>
                <Col md={4}></Col>
                <Col md={4} className='text-center'>
                    <motion.img
                        src={require('../../assets/images/secretCollection.png')}
                        alt="sort"
                        className='ms-5 cursor-pointer'
                        initial={{ y: -50, opacity: 0 }}
                        animate={{ y: 0, opacity: 1 }}
                        transition={{ duration: 2, delay: 0.5 }}
                    />
                </Col>
                <Col md={4} className='d-flex align-items-end justify-content-center'>
                    <motion.div
                        initial={{ y: 50, opacity: 0 }}
                        animate={{ y: 0, opacity: 1 }}
                        transition={{ duration: 2, delay: 0.5 }}
                    >
                        <Dropdown>
                            <Dropdown.Toggle id="dropdown-basic" className='dropdown-image'>
                                <img
                                    src={require('../../assets/images/filters.png')}
                                    alt="filter"
                                    className='ms-3 me-5 cursor-pointer'
                                />
                            </Dropdown.Toggle>

                            <Dropdown.Menu style={{ width: '400px', padding: '10px', background: '#f8f8f8', borderColor: '#e8e8e8' }}>
                                <div style={{ display: 'flex' }}>
                                    <div style={{ display: 'flex', flexDirection: 'column', flex: '1' }}>
                                        <p className='cursor-pointer mini-text'><RiWomenLine className='me-2' />WOMEN</p>
                                        <p className='cursor-pointer mini-text'><AiOutlineWoman className='me-2' />MEN</p>
                                    </div>
                                    <div style={{ display: 'flex', flexDirection: 'column', flex: '1' }}>
                                        <p className='cursor-pointer mini-text'><LiaTshirtSolid className='me-2' />SHORTS</p>
                                        <p className='cursor-pointer mini-text'><HiOutlineShoppingBag className='me-2' />BAGS</p>
                                        <p className='cursor-pointer mini-text'><GiBigDiamondRing className='me-2' />JEWELERY</p>
                                    </div></div>
                            </Dropdown.Menu>
                        </Dropdown>
                    </motion.div>

                    <motion.div
                        initial={{ y: 50, opacity: 0 }}
                        animate={{ y: 0, opacity: 1 }}
                        transition={{ duration: 2, delay: 1 }} // Ajout d'un délai pour l'animation du deuxième Dropdown
                    >
                        <Dropdown>
                            <Dropdown.Toggle id="dropdown-basic" className='dropdown-image'>
                                <img
                                    src={require('../../assets/images/sort.png')}
                                    alt="sort"
                                    className='ms-5 cursor-pointer'
                                />
                            </Dropdown.Toggle>

                            <Dropdown.Menu style={{ width: '200px', padding: '10px', background: '#f8f8f8', borderColor: '#e8e8e8' }}>
                                <div style={{ display: 'flex' }}>
                                    <div style={{ display: 'flex', flexDirection: 'column', flex: '1' }}>
                                        <p className='cursor-pointer mini-text'>BY PRICE ASC</p>
                                        <p className='cursor-pointer mini-text'>BY PRICE DESC</p>
                                    </div></div>
                            </Dropdown.Menu>
                        </Dropdown>
                    </motion.div>
                </Col>


            </Row>
            <Row className='d-flex justify-content-center mt-5' >
                <Col md={8}>
                    <ProductSlider products={products} />
                    <div className='d-flex justify-content-center'>
                        <Button className='btn-cart mx-2' onClick={addToCart}>ADD TO CART <CartLogo className='ms-5' /> </Button>
                    </div>

                </Col>
                <Cart show={show} setShow={setShow} product={products[0]} totalProduct={totalProduct} setTotalProduct={setTotalProduct} />
            </Row>
        </div>
    )

}
export default Home