import Offcanvas from 'react-bootstrap/Offcanvas';
import { Button, Col, Row } from 'react-bootstrap';

import { ReactComponent as CartProduct } from '../../assets/svg/cart.svg';
import { ReactComponent as MiniProduct } from '../../assets/svg/miniProduct.svg';
import { ReactComponent as MenShirt } from '../../assets/svg/menShirt.svg';
import { ReactComponent as Close } from '../../assets/svg/closeBtn.svg';
import { ReactComponent as Trash } from '../../assets/svg/trash.svg';
import { ReactComponent as Line } from '../../assets/svg/line.svg';
import { ReactComponent as Taxes } from '../../assets/svg/taxes.svg';
import { ReactComponent as Total } from '../../assets/svg/total.svg';

const Cart = (props) => {

    //Fonction pour gérer la fermeture d'un composant Offcanvas.
    const handleClose = () => setShow(false);

    // Destructuration des propriétés passées en tant que paramètres de composant.
    const { show, setShow, product, totalProduct, setTotalProduct } = props;

    return (
        product &&
        <Offcanvas show={show} placement={'end'} className='custom-modal'>
            <Offcanvas.Header>
                <Close className='cursor-pointer' onClick={handleClose} />
                <div className='d-flex flex-row'>
                    <p className='mt-4 total'>{totalProduct}</p>
                    <CartProduct />
                </div>
            </Offcanvas.Header>
            <Offcanvas.Body>

                <>
                    {totalProduct > 0 && <Row className='d-flex justify-content-center mt-5'>
                        <Col md="5">
                            <MiniProduct />
                        </Col>
                        <Col md="7" className='mt-5'>
                            <Row>
                                <MenShirt />
                            </Row>
                            <Row className='mt-2 mt-5 '>
                                <Col xs={10}>
                                    <p className='mini-price text-end'>{Number(product.price).toLocaleString('fr-FR', {
                                        style: 'currency',
                                        currency: 'EUR',
                                        minimumFractionDigits: 0,
                                        maximumFractionDigits: 0,
                                    })} </p>
                                </Col>
                                <Col xs={2}>
                                    <Trash onClick={() => setTotalProduct(0)} className='cursor-pointer' />
                                </Col>

                            </Row>
                            <Row className='mt-5 '>
                                <Col md={10}>
                                    <p className='text-end'>
                                        <img onClick={() => setTotalProduct((prev) => prev > 0 ? prev - 1 : 0)} src={require('../../assets/images/mines.png')} alt="filter" className='me-3 cursor-pointer' />
                                        {totalProduct} ITEM
                                        <img onClick={() => setTotalProduct((prev) => prev < 9 ? prev + 1 : prev)} src={require('../../assets/images/plus.png')} alt="filter" className='ms-3 cursor-pointer' /></p>
                                </Col>
                            </Row>
                            <Row className='mt-2'>
                                <Line />
                            </Row>
                        </Col>
                    </Row>}



                    {totalProduct > 0 && (
                        <Row className='mt-5'>
                            <div className='d-flex flex-row align-items-center'>
                                <Col xs={7} className='d-flex justify-content-end'>
                                    <Taxes className='text-end' />
                                </Col>
                                <Col xs={5}>
                                    <p className='medium-price ms-5'> {Number('3166.67').toLocaleString('fr-FR', {
                                        style: 'currency',
                                        currency: 'EUR',
                                        minimumFractionDigits: 0,
                                        maximumFractionDigits: 2,
                                    })}   </p>
                                </Col>
                            </div>
                        </Row>
                    )}
                    <Row className='mt-4'>
                        <Col xs={7} className='d-flex justify-content-end'>
                            <Total className='text-end' />
                        </Col>
                        <Col xs={5}>
                            <p className='large-price ms-5 mt-2'> {Number(product.price * totalProduct).toLocaleString('fr-FR', {
                                style: 'currency',
                                currency: 'EUR',
                                minimumFractionDigits: 0,
                                maximumFractionDigits: 2,
                            })}   </p>
                        </Col>
                    </Row>
                    <Row className='d-flex justify-content-center'>
                        <Button className='btn-checkout mx-1 mt-5'>GO TO CHECKOUT</Button>
                    </Row>
                </>
            </Offcanvas.Body>
        </Offcanvas>
    )
}
export default Cart