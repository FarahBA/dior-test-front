import { Carousel } from 'react-responsive-carousel';

import { ReactComponent as LeftArrow } from '../../assets/svg/leftArrow.svg';
import { ReactComponent as RightArrow } from '../../assets/svg/rightArrow.svg';
import { useMediaQuery } from '@mui/material';
import { useState } from 'react';

// Styles personnalisés pour les flèches de navigation du carrousel
const customArrowStyles = {
  position: 'absolute',
  top: '50%',
  transform: 'translateY(-50%)',
  zIndex: 2,
  cursor: 'pointer',
  background: 'none',
  border: 'none',
};

const ProductSlider = (props) => {
  // Extraction des produits des propriétés passées
  const { products } = props;

  // État pour gérer l'indice du produit actuellement affiché
  const [currentIndex, setCurrentIndex] = useState(0);

  // Utilisation de la fonction useMediaQuery pour détecter si l'écran est de type bureau (desktop)
  const isDesktop = useMediaQuery('(min-width: 1025px)');

    return (
        <Carousel
        showArrows={false}
        infiniteLoop={true}
        showStatus={false}
        showThumbs={false}
        autoFocus={true}
        showIndicators={false}
        centerMode={true}
        centerSlidePercentage={70}
        renderThumbs={() => {}}
        renderArrowPrev={(onClickHandler, label) =>
          isDesktop && (
            <button onClick={onClickHandler} title={label} style={{ ...customArrowStyles, left: 0 }}>
              <LeftArrow />
            </button>
          )
        }
        renderArrowNext={(onClickHandler, label) =>
          isDesktop && (
            <button onClick={onClickHandler} title={label} style={{ ...customArrowStyles, right: 0 }}>
              <RightArrow />
            </button>
          )
        }
        onChange={(index) => setCurrentIndex(index)}
        selectedItem={currentIndex}
      >
        {products.map((item, index) => (
          <div key={item.id} style={{ opacity: index === currentIndex ? 1 : 0.5, width: index === currentIndex ? '100%' : '90%' }}>
            <img src={require(`../../assets/images/${item.image}`)} alt="sort" className='ms-5 cursor-pointer' />
          </div>
        ))}
      </Carousel>
    )
}

export default ProductSlider