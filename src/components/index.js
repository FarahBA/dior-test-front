// Importez les composants depuis leurs fichiers respectifs
import Home from './products/Home';
import Login from './login/Login';

// Exportez les composants pour les rendre disponibles à l'extérieur
export { Home, Login };