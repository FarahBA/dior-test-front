import { ApolloClient, InMemoryCache, createHttpLink } from "@apollo/client";

// URL de l'API GraphQL
export const API_URL = 'http://localhost:3001/api';

// Création d'une instance du client Apollo
const client = new ApolloClient({
    link: createHttpLink({ uri: API_URL  }), // Configuration de l'URL de l'API GraphQL
    cache: new InMemoryCache(), // Utilisation d'un cache en mémoire pour stocker les données
});

export default client;