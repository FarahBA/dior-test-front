import { gql } from "@apollo/client";

// Définition de la mutation GraphQL pour l'authentification de l'utilisateur
const LOGIN_USER = gql`
  mutation LoginUser($username: String!, $password: String!) {
    login(username: $username, password: $password) {
      message
      user {
        id
        username
      }
    }
  }
`;

export { LOGIN_USER };